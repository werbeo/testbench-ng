package org.rapidpm.vaadin.addons.testbench.junit5.pageobject.vaadin;

import org.rapidpm.vaadin.addons.testbench.junit5.pageobject.GenericVaadinAppSpecific;

public interface VaadinPageObject extends GenericVaadinAppSpecific {
}
